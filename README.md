# Lab6 -- Mutation testing 

# report

# mutatest run from lab
```zsh
➜  s23-lab6-mutation-testing git:(main) mutatest --src ./src           
2023-04-12 19:55:13,230: Running clean trial
================================ test session starts =================================
platform darwin -- Python 3.9.6, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/tanugayev/dev/inno/s23-lab6-mutation-testing
plugins: anyio-3.6.2
collected 13 items                                                                   

src/test_calculator.py .............                                           [100%]

================================= 13 passed in 0.03s =================================
2023-04-12 19:55:13,633: 7 mutation targets found in src/bonus_system.py AST.
2023-04-12 19:55:13,635: 14 mutation targets found in src/calculator.py AST.
2023-04-12 19:55:13,635: Setting random.seed to: None
2023-04-12 19:55:13,635: Coverage file does not exist, proceeding to sample from all targets.
2023-04-12 19:55:13,635: Total sample space size: 21
2023-04-12 19:55:13,635: Selecting 10 locations from 21 potentials.
2023-04-12 19:55:13,635: Starting individual mutation trials!
2023-04-12 19:55:13,635: Running serial (single processor) dispatch mode.
2023-04-12 19:55:13,635: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=5, col_offset=24, op_type=<class 'ast.Eq'>, end_lineno=5, end_col_offset=44)
2023-04-12 19:55:13,799: Result: Survived at src/bonus_system.py: (5, 24)
2023-04-12 19:55:13,799: Break on survival: stopping further mutations at location.
2023-04-12 19:55:13,799: Current target location: calculator.py, LocIndex(ast_class='Compare', lineno=22, col_offset=11, op_type=<class 'ast.Eq'>, end_lineno=22, end_col_offset=20)
2023-04-12 19:55:13,986: Result: Detected at src/calculator.py: (22, 11)
2023-04-12 19:55:14,147: Result: Survived at src/calculator.py: (22, 11)
2023-04-12 19:55:14,148: Break on survival: stopping further mutations at location.
2023-04-12 19:55:14,148: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=6, col_offset=24, op_type=<class 'ast.Eq'>, end_lineno=6, end_col_offset=44)
2023-04-12 19:55:14,309: Result: Survived at src/bonus_system.py: (6, 24)
2023-04-12 19:55:14,309: Break on survival: stopping further mutations at location.
2023-04-12 19:55:14,309: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=8, col_offset=19, op_type=<class 'ast.Lt'>, end_lineno=8, end_col_offset=33)
2023-04-12 19:55:14,461: Result: Survived at src/bonus_system.py: (8, 19)
2023-04-12 19:55:14,461: Break on survival: stopping further mutations at location.
2023-04-12 19:55:14,461: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=7, col_offset=17, op_type=<class 'ast.Lt'>, end_lineno=7, end_col_offset=31)
2023-04-12 19:55:14,616: Result: Survived at src/bonus_system.py: (7, 17)
2023-04-12 19:55:14,616: Break on survival: stopping further mutations at location.
2023-04-12 19:55:14,616: Current target location: calculator.py, LocIndex(ast_class='Compare', lineno=21, col_offset=9, op_type=<class 'ast.Eq'>, end_lineno=21, end_col_offset=20)
2023-04-12 19:55:14,800: Result: Detected at src/calculator.py: (21, 9)
2023-04-12 19:55:14,966: Result: Survived at src/calculator.py: (21, 9)
2023-04-12 19:55:14,966: Break on survival: stopping further mutations at location.
2023-04-12 19:55:14,966: Current target location: calculator.py, LocIndex(ast_class='BinOp', lineno=12, col_offset=11, op_type=<class 'ast.Sub'>, end_lineno=12, end_col_offset=22)
2023-04-12 19:55:15,145: Result: Detected at src/calculator.py: (12, 11)
2023-04-12 19:55:17,160: Result: Timeout at src/calculator.py: (12, 11)
2023-04-12 19:55:17,161: Break on timeout: stopping further mutations at location.
2023-04-12 19:55:17,161: Current target location: calculator.py, LocIndex(ast_class='BinOp', lineno=3, col_offset=11, op_type=<class 'ast.Add'>, end_lineno=3, end_col_offset=36)
2023-04-12 19:55:17,341: Result: Detected at src/calculator.py: (3, 11)
2023-04-12 19:55:17,531: Result: Detected at src/calculator.py: (3, 11)
2023-04-12 19:55:17,705: Result: Detected at src/calculator.py: (3, 11)
2023-04-12 19:55:17,879: Result: Detected at src/calculator.py: (3, 11)
2023-04-12 19:55:18,052: Result: Detected at src/calculator.py: (3, 11)
2023-04-12 19:55:18,239: Result: Detected at src/calculator.py: (3, 11)
2023-04-12 19:55:18,239: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=9, col_offset=17, op_type=<class 'ast.Lt'>, end_lineno=9, end_col_offset=32)
2023-04-12 19:55:18,393: Result: Survived at src/bonus_system.py: (9, 17)
2023-04-12 19:55:18,393: Break on survival: stopping further mutations at location.
2023-04-12 19:55:18,393: Current target location: calculator.py, LocIndex(ast_class='If', lineno=22, col_offset=8, op_type='If_Statement', end_lineno=24, end_col_offset=22)
2023-04-12 19:55:18,568: Result: Detected at src/calculator.py: (22, 8)
2023-04-12 19:55:18,741: Result: Detected at src/calculator.py: (22, 8)
2023-04-12 19:55:18,742: Running clean trial
================================ test session starts =================================
platform darwin -- Python 3.9.6, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/tanugayev/dev/inno/s23-lab6-mutation-testing
plugins: anyio-3.6.2
collected 13 items                                                                   

src/test_calculator.py .............                                           [100%]

================================= 13 passed in 0.01s =================================
2023-04-12 19:55:18,898: CLI Report:
Mutatest diagnostic summary
===========================
 - Source location: /Users/tanugayev/dev/inno/s23-lab6-mutation-testing/src
 - Test commands: ['pytest']
 - Mode: s
 - Excluded files: []
 - N locations input: 10
 - Random seed: None

Random sample details
---------------------
 - Total locations mutated: 10
 - Total locations identified: 21
 - Location sample coverage: 47.62 %

Running time details
--------------------
 - Clean trial 1 run time: 0:00:00.401448
 - Clean trial 2 run time: 0:00:00.155980
 - Mutation trials total run time: 0:00:05.109119

2023-04-12 19:55:18,898: Trial Summary Report:

Overall mutation trial summary
==============================
 - SURVIVED: 7
 - DETECTED: 11
 - TIMEOUT: 1
 - TOTAL RUNS: 19
 - RUN DATETIME: 2023-04-12 19:55:18.898634

2023-04-12 19:55:18,898: Detected mutations:

DETECTED
--------
 - src/calculator.py: (l: 3, c: 11) - mutation from <class 'ast.Add'> to <class 'ast.Pow'>
 - src/calculator.py: (l: 3, c: 11) - mutation from <class 'ast.Add'> to <class 'ast.Div'>
 - src/calculator.py: (l: 3, c: 11) - mutation from <class 'ast.Add'> to <class 'ast.FloorDiv'>
 - src/calculator.py: (l: 3, c: 11) - mutation from <class 'ast.Add'> to <class 'ast.Sub'>
 - src/calculator.py: (l: 3, c: 11) - mutation from <class 'ast.Add'> to <class 'ast.Mod'>
 - src/calculator.py: (l: 3, c: 11) - mutation from <class 'ast.Add'> to <class 'ast.Mult'>
 - src/calculator.py: (l: 12, c: 11) - mutation from <class 'ast.Sub'> to <class 'ast.Mult'>
 - src/calculator.py: (l: 21, c: 9) - mutation from <class 'ast.Eq'> to <class 'ast.Lt'>
 - src/calculator.py: (l: 22, c: 8) - mutation from If_Statement to If_True
 - src/calculator.py: (l: 22, c: 8) - mutation from If_Statement to If_False
 - src/calculator.py: (l: 22, c: 11) - mutation from <class 'ast.Eq'> to <class 'ast.GtE'>

2023-04-12 19:55:18,899: Timedout mutations:

TIMEOUT
-------
 - src/calculator.py: (l: 12, c: 11) - mutation from <class 'ast.Sub'> to <class 'ast.Pow'>

2023-04-12 19:55:18,899: Surviving mutations:

SURVIVED
--------
 - src/bonus_system.py: (l: 5, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.Gt'>
 - src/bonus_system.py: (l: 6, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.LtE'>
 - src/bonus_system.py: (l: 7, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.NotEq'>
 - src/bonus_system.py: (l: 8, c: 19) - mutation from <class 'ast.Lt'> to <class 'ast.GtE'>
 - src/bonus_system.py: (l: 9, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.GtE'>
 - src/calculator.py: (l: 21, c: 9) - mutation from <class 'ast.Eq'> to <class 'ast.LtE'>
 - src/calculator.py: (l: 22, c: 11) - mutation from <class 'ast.Eq'> to <class 'ast.LtE'>
```

# mutatest run after investigation and correcting:

```zsh

➜  s23-lab6-mutation-testing git:(main) ✗ mutatest --src ./src -t "pytest src/test_bonus_system.py" -x 1 --src ./src/bonus_system.py
2023-04-12 22:49:30,081: Running clean trial
================================ test session starts =================================
platform darwin -- Python 3.9.6, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/tanugayev/dev/inno/s23-lab6-mutation-testing
plugins: anyio-3.6.2
collected 44 items                                                                   

src/test_bonus_system.py ............................................          [100%]

================================= 44 passed in 0.03s =================================
2023-04-12 22:49:30,249: 7 mutation targets found in src/bonus_system.py AST.
2023-04-12 22:49:30,249: Setting random.seed to: None
2023-04-12 22:49:30,249: Coverage file does not exist, proceeding to sample from all targets.
2023-04-12 22:49:30,249: Total sample space size: 7
2023-04-12 22:49:30,249: 10 exceeds sample space, using full sample: 7.
2023-04-12 22:49:30,249: Starting individual mutation trials!
2023-04-12 22:49:30,249: Running serial (single processor) dispatch mode.
2023-04-12 22:49:30,249: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=4, col_offset=24, op_type=<class 'ast.Eq'>, end_lineno=4, end_col_offset=45)
2023-04-12 22:49:30,487: Result: Detected at src/bonus_system.py: (4, 24)
2023-04-12 22:49:30,716: Result: Detected at src/bonus_system.py: (4, 24)
2023-04-12 22:49:30,945: Result: Detected at src/bonus_system.py: (4, 24)
2023-04-12 22:49:31,204: Result: Detected at src/bonus_system.py: (4, 24)
2023-04-12 22:49:31,419: Result: Detected at src/bonus_system.py: (4, 24)
2023-04-12 22:49:31,419: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=5, col_offset=24, op_type=<class 'ast.Eq'>, end_lineno=5, end_col_offset=44)
2023-04-12 22:49:31,642: Result: Detected at src/bonus_system.py: (5, 24)
2023-04-12 22:49:31,882: Result: Detected at src/bonus_system.py: (5, 24)
2023-04-12 22:49:32,107: Result: Detected at src/bonus_system.py: (5, 24)
2023-04-12 22:49:32,330: Result: Detected at src/bonus_system.py: (5, 24)
2023-04-12 22:49:32,571: Result: Detected at src/bonus_system.py: (5, 24)
2023-04-12 22:49:32,571: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=6, col_offset=24, op_type=<class 'ast.Eq'>, end_lineno=6, end_col_offset=44)
2023-04-12 22:49:32,789: Result: Detected at src/bonus_system.py: (6, 24)
2023-04-12 22:49:33,018: Result: Detected at src/bonus_system.py: (6, 24)
2023-04-12 22:49:33,240: Result: Detected at src/bonus_system.py: (6, 24)
2023-04-12 22:49:33,485: Result: Detected at src/bonus_system.py: (6, 24)
2023-04-12 22:49:33,712: Result: Detected at src/bonus_system.py: (6, 24)
2023-04-12 22:49:33,712: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=7, col_offset=17, op_type=<class 'ast.Lt'>, end_lineno=7, end_col_offset=31)
2023-04-12 22:49:33,943: Result: Detected at src/bonus_system.py: (7, 17)
2023-04-12 22:49:34,155: Result: Detected at src/bonus_system.py: (7, 17)
2023-04-12 22:49:34,367: Result: Detected at src/bonus_system.py: (7, 17)
2023-04-12 22:49:34,556: Result: Detected at src/bonus_system.py: (7, 17)
2023-04-12 22:49:34,764: Result: Detected at src/bonus_system.py: (7, 17)
2023-04-12 22:49:34,764: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=8, col_offset=19, op_type=<class 'ast.Lt'>, end_lineno=8, end_col_offset=33)
2023-04-12 22:49:34,956: Result: Detected at src/bonus_system.py: (8, 19)
2023-04-12 22:49:35,162: Result: Detected at src/bonus_system.py: (8, 19)
2023-04-12 22:49:35,362: Result: Detected at src/bonus_system.py: (8, 19)
2023-04-12 22:49:35,569: Result: Detected at src/bonus_system.py: (8, 19)
2023-04-12 22:49:35,763: Result: Detected at src/bonus_system.py: (8, 19)
2023-04-12 22:49:35,763: Current target location: bonus_system.py, LocIndex(ast_class='Compare', lineno=9, col_offset=17, op_type=<class 'ast.Lt'>, end_lineno=9, end_col_offset=32)
2023-04-12 22:49:35,948: Result: Detected at src/bonus_system.py: (9, 17)
2023-04-12 22:49:36,146: Result: Detected at src/bonus_system.py: (9, 17)
2023-04-12 22:49:36,333: Result: Detected at src/bonus_system.py: (9, 17)
2023-04-12 22:49:36,532: Result: Detected at src/bonus_system.py: (9, 17)
2023-04-12 22:49:36,724: Result: Detected at src/bonus_system.py: (9, 17)
2023-04-12 22:49:36,725: Current target location: bonus_system.py, LocIndex(ast_class='BinOp', lineno=10, col_offset=11, op_type=<class 'ast.Mult'>, end_lineno=10, end_col_offset=29)
2023-04-12 22:49:36,964: Result: Detected at src/bonus_system.py: (10, 11)
2023-04-12 22:49:37,207: Result: Detected at src/bonus_system.py: (10, 11)
2023-04-12 22:49:37,459: Result: Detected at src/bonus_system.py: (10, 11)
2023-04-12 22:49:37,722: Result: Detected at src/bonus_system.py: (10, 11)
2023-04-12 22:49:37,982: Result: Detected at src/bonus_system.py: (10, 11)
2023-04-12 22:49:38,229: Result: Detected at src/bonus_system.py: (10, 11)
2023-04-12 22:49:38,229: Running clean trial
================================ test session starts =================================
platform darwin -- Python 3.9.6, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/tanugayev/dev/inno/s23-lab6-mutation-testing
plugins: anyio-3.6.2
collected 44 items                                                                   

src/test_bonus_system.py ............................................          [100%]

================================= 44 passed in 0.03s =================================
2023-04-12 22:49:38,390: CLI Report:

Mutatest diagnostic summary
===========================
 - Source location: /Users/tanugayev/dev/inno/s23-lab6-mutation-testing/src/bonus_system.py
 - Test commands: ['pytest', 'src/test_bonus_system.py']
 - Mode: s
 - Excluded files: []
 - N locations input: 10
 - Random seed: None

Random sample details
---------------------
 - Total locations mutated: 7
 - Total locations identified: 7
 - Location sample coverage: 100.00 %


Running time details
--------------------
 - Clean trial 1 run time: 0:00:00.166623
 - Clean trial 2 run time: 0:00:00.160303
 - Mutation trials total run time: 0:00:07.981100

2023-04-12 22:49:38,390: Trial Summary Report:

Overall mutation trial summary
==============================
 - DETECTED: 36
 - TOTAL RUNS: 36
 - RUN DATETIME: 2023-04-12 22:49:38.390487

2023-04-12 22:49:38,390: Detected mutations:

DETECTED
--------
 - src/bonus_system.py: (l: 4, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.Lt'>
 - src/bonus_system.py: (l: 4, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.LtE'>
 - src/bonus_system.py: (l: 4, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.Gt'>
 - src/bonus_system.py: (l: 4, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.NotEq'>
 - src/bonus_system.py: (l: 4, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.GtE'>
 - src/bonus_system.py: (l: 5, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.GtE'>
 - src/bonus_system.py: (l: 5, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.Lt'>
 - src/bonus_system.py: (l: 5, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.Gt'>
 - src/bonus_system.py: (l: 5, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.LtE'>
 - src/bonus_system.py: (l: 5, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.NotEq'>
 - src/bonus_system.py: (l: 6, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.Lt'>
 - src/bonus_system.py: (l: 6, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.Gt'>
 - src/bonus_system.py: (l: 6, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.LtE'>
 - src/bonus_system.py: (l: 6, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.NotEq'>
 - src/bonus_system.py: (l: 6, c: 24) - mutation from <class 'ast.Eq'> to <class 'ast.GtE'>
 - src/bonus_system.py: (l: 7, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.GtE'>
 - src/bonus_system.py: (l: 7, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.NotEq'>
 - src/bonus_system.py: (l: 7, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.Eq'>
 - src/bonus_system.py: (l: 7, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.LtE'>
 - src/bonus_system.py: (l: 7, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.Gt'>
 - src/bonus_system.py: (l: 8, c: 19) - mutation from <class 'ast.Lt'> to <class 'ast.LtE'>
 - src/bonus_system.py: (l: 8, c: 19) - mutation from <class 'ast.Lt'> to <class 'ast.GtE'>
 - src/bonus_system.py: (l: 8, c: 19) - mutation from <class 'ast.Lt'> to <class 'ast.NotEq'>
 - src/bonus_system.py: (l: 8, c: 19) - mutation from <class 'ast.Lt'> to <class 'ast.Gt'>
 - src/bonus_system.py: (l: 8, c: 19) - mutation from <class 'ast.Lt'> to <class 'ast.Eq'>
 - src/bonus_system.py: (l: 9, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.LtE'>
 - src/bonus_system.py: (l: 9, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.GtE'>
 - src/bonus_system.py: (l: 9, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.NotEq'>
 - src/bonus_system.py: (l: 9, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.Eq'>
 - src/bonus_system.py: (l: 9, c: 17) - mutation from <class 'ast.Lt'> to <class 'ast.Gt'>
 - src/bonus_system.py: (l: 10, c: 11) - mutation from <class 'ast.Mult'> to <class 'ast.Add'>
 - src/bonus_system.py: (l: 10, c: 11) - mutation from <class 'ast.Mult'> to <class 'ast.Div'>
 - src/bonus_system.py: (l: 10, c: 11) - mutation from <class 'ast.Mult'> to <class 'ast.FloorDiv'>
 - src/bonus_system.py: (l: 10, c: 11) - mutation from <class 'ast.Mult'> to <class 'ast.Pow'>
 - src/bonus_system.py: (l: 10, c: 11) - mutation from <class 'ast.Mult'> to <class 'ast.Mod'>
 - src/bonus_system.py: (l: 10, c: 11) - mutation from <class 'ast.Mult'> to <class 'ast.Sub'>

2023-04-12 22:49:38,391: Timedout mutations:

2023-04-12 22:49:38,391: Surviving mutations:

2023-04-12 22:49:38,391: Survivor tolerance check for 1 surviving mutants.
2023-04-12 22:49:38,391: Survivor tolerance OK: 0 / 1
```

Result: No mutation survived!